# AngularTesting

This project is a testing fro serveral features of Angular.

In this commit the project use:

Features:

- [x] Custom Components
- [x] Custom Services
- [x] Custom Directive
- [x] Route
- [x] Route Parameters : required param
- [x] Route Parameters : query param
- [x] Route Lazy Loading
- [x] Form Validation by Template-driven
- [x] Form Validation by Reactive Forms
- [x] Custom Components with forms integration (template and reactive)
- [x] Load JSON in assets by http client
- [x] Custom SCSS
- [x] Data base inside browser

I also want to use several other features like:

Features to-do:

- [ ] Jasmine unit Test
- [ ] trackBy
- [ ] Angular Animations
- [ ] Guards
- [ ] Authentification local
- [ ] Authentification with webservices
- [ ] Data base in webservices
- [ ] Custom PipeLine
- [ ] Angular Material UI (branch)
- [ ] Bootstrap IU (branch)



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
