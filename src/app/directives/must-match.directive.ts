import { Directive, Input } from '@angular/core';
import { AbstractControl, FormGroup, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';

@Directive({
  selector: '[mustMatch]',
  providers: [{ provide: NG_VALIDATORS, useExisting: MustMatchDirective, multi: true }]
})
export class MustMatchDirective implements Validator {

  @Input('mustMatch') mustMatch: string = "";

  validate(c: AbstractControl): ValidationErrors {
    return this.mustMatchValidator(c.value, this.mustMatch, c);
  }
  

  private mustMatchValidator(phrase1: string, phrase2: string, control: AbstractControl): ValidationErrors {
    // set error on matchingControl if validation fails
    if (phrase1 != phrase2){
      return { noMatch : true }
    }

    return {};

  }
}

export class MustMatchReactiveDirective {
  static match(): ValidatorFn {
    return (control: AbstractControl) => {
      // set error on matchingControl if validation fails
      if (control.get('password1')?.value != control.get('password2')?.value){
        control.get('password2')?.setErrors({ noMatch : true });
        return { noMatch : true }
      }
  
      return {};
    };
  }
}