import { TestBed } from '@angular/core/testing';

import { HashSha256Service } from './hash-sha256.service';

describe('HashSha256Service', () => {
  let service: HashSha256Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HashSha256Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('test abc value', () =>{
    expect(service.hex_sha256("abc").toLowerCase()).toBe("ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad");
  });
});
