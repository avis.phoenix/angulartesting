import { TestBed } from '@angular/core/testing';

import { UsersService } from './users.service';

describe('UsersServiceService', () => {
  let service: UsersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UsersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('add two users', () => {
    let user1 = {
      username: "User 1",
      phoneNumber: "123456789",
      email: "email@mail.com",
      country: "Unites States",
      state: "Alabama",
      password: "123456",
    };
    let user2 = {
      username: "User 2",
      phoneNumber: "(12)3456789",
      email: "another@mail.com",
      country: "India",
      state: "Otro",
      password: "123.456",
    };
    let firstLen = service.usersCount();
    service.addUser(user1);
    service.addUser(user2);
    let copyUsers = service.getCurrentUsers();
    let len = copyUsers.length;

    expect(service.usersCount()).toEqual(firstLen+2);
    expect(copyUsers[len-2]).toEqual(user1);
    expect(copyUsers[len-1]).toEqual(user2);
    
  });

  it('delete a user', () => {
    let count = service.usersCount();
    if (count < 2){
      let user1 = {
        username: "User 1",
        phoneNumber: "123456789",
        email: "email@mail.com",
        country: "Unites States",
        state: "Alabama",
        password: "123456",
      };
      let user2 = {
        username: "User 2",
        phoneNumber: "(12)3456789",
        email: "another@mail.com",
        country: "India",
        state: "Otro",
        password: "123.456",
      };
      service.addUser(user1);
      service.addUser(user2);

      count = service.usersCount();
    }
    let user1 = service.getCurrentUsers()[0];
    service.deleteUser(count-1);
    expect(service.usersCount()).toEqual(count-1);
    expect(service.getCurrentUsers()[0]).toEqual(user1);
  });
  
});
