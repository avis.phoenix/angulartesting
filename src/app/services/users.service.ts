import { Injectable } from '@angular/core';
import { UserData } from '../interfaces/user-data.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private users: Array<UserData> | undefined;
  private nameLocalStorage = "usersList";

  constructor() { }

  getCurrentUsers(): Array<UserData> { return this.users ? Array.from(this.users) : []; }

  addUser(user: UserData) : void{
    if (!user) return;

    if (!this.users){
      this.users = [];
    }
    this.users.push(user);
  }

  getUser(index: number): UserData {
    if (this.users && index >= 0 && index < this.users?.length){
      return this.users[index];
    }
    return {username:"",country:"",email:"",password: "", phoneNumber: "", state: ""};
  }

  updateUser(index:number, user: UserData) : void{
    if (this.users && index >= 0 && index < this.users?.length && user){
      Object.assign(this.users[index],user);
    }
  }

  deleteUser(index: number): void{
    if (this.users && index >= 0 && index <= this.users?.length){
      this.users.splice(index,1);
    }
  }

  usersCount(): number{
    return this.users ? this.users.length : 0;
  }

  loadFromLocalStorage(): void{
    let value = localStorage.getItem(this.nameLocalStorage);
    if (value){
      this.users = JSON.parse(value);
    }else{
      throw "No array";
    }
  }

  saveToLocalStorage(): void{
    localStorage.setItem(this.nameLocalStorage,JSON.stringify(this.users));
  }


}
