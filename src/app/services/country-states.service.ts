import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Country } from '../interfaces/country.model';
import { State } from '../interfaces/state.model';

@Injectable({
  providedIn: 'root'
})
export class CountryStatesService {

  private urlData = "/assets/data.json";

  constructor(private http: HttpClient) { }

  public getCountries(): Observable<Country[]> {
    return new Observable<Country[]>( (observer)=>{
      this.http.get<any>(this.urlData).subscribe(
        (data)=>{ observer.next(data["countries"])},
        (err)=>{ observer.error(err) });
    });
  }

  public getStates(countryId: number): Observable<State[]> {
    return new Observable<State[]>( (observer)=>{
      this.http.get<any>(this.urlData).subscribe(
        (data)=>{ observer.next(data["states"].filter( (item: State) => item.countryId == countryId))},
        (err)=>{ observer.error(err) });
    });
  }
}
