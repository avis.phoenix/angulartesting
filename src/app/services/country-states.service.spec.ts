import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CountryStatesService } from './country-states.service';

describe('CountrystatesServiceService', () => {
  let service: CountryStatesService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        CountryStatesService,
      ],
    });
    service = TestBed.inject(CountryStatesService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getCountries() should fetch and return countries from assets/data.json', (done) => {
    const countries = [
      { id: 1, name: 'United States' },
      { id: 2, name: 'India' },
      { id: 4, name: 'New Zealand' },
    ];

    service.getCountries().subscribe((res: any) => {
      expect(res).toEqual(countries);
      done();
    });

    const req = httpMock.expectOne('/assets/data.json', 'countries from assets/data.json');
    expect(req.request.method).toBe('GET');
    req.flush({ countries });
  });

  it('getStates() should fetch and return states for given country from assets/data.json', (done) => {
    const states = [
      { id: 10, countryId: 1, name: 'Florida' },
      { id: 12, countryId: 1, name: 'Hawaii' },
      { id: 13, countryId: 1, name: 'Idaho' },
      { id: 14, countryId: 1, name: 'Illinois', },
      { id: 15, countryId: 1, name: 'Indiana' },
    ];

    service.getStates(1).subscribe((res: any) => {
      expect(res).toEqual(states);
      done();
    });

    const req = httpMock.expectOne('/assets/data.json', 'states from assets/data.json');
    expect(req.request.method).toBe('GET');
    req.flush({ states });
  });

});
