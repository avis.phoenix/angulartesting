export interface NotificationItem {
    msg: string;
    type: string;
    time: number;
}