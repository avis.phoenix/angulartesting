export interface UserData {
  username: string;
  phoneNumber: string;
  email: string;
  country: string;
  state: string;
  password: string;
}
