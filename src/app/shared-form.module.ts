import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PasswordInputComponent } from './components/password-input/password-input.component';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  declarations: [
    PasswordInputComponent
  ],
  exports: [PasswordInputComponent]
})
export class SharedFormModule { }