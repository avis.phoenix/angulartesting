import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './pages/form/form.component';

const routes: Routes = [
  { path:'', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule) },
  { path:'form/reactive',  loadChildren: () => import('./pages/form/form.module').then(m => m.FormModule) },
  { path:'form/template',  loadChildren: () => import('./pages/template-form/template-form.module').then(m => m.TemplateFormModule) },
  { path:'view/:id',  loadChildren: () => import('./pages/display-data/display-data.module').then(m => m.DisplayDataModule) },
  { path:'manage/users',  loadChildren: () => import('./pages/manage-users/manage-users.module').then(m => m.ManageUsersModule) },
  { path:'service/:action/:type', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule) },
  { path: '**',  loadChildren: () => import('./pages/not-found/not-found.module').then(m => m.NotFoundModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
