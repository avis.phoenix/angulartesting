import { UserData } from "src/app/interfaces/user-data.model";

export class UsersServiceStub {
    users: Array<UserData> = [{
      username: 'john',
      email: 'john.doe@devskiller.com',
      country: 'United States',
      state: 'Delaware',
      phoneNumber: '(999) 999-9999',
      password: "123456"
    }];
  
    getCurrentUsers(): Array<UserData> {
      return this.users;
    }
  
    addUser(user: UserData) : void{}
    getUser(index: number): UserData {
      return this.users[0];
    }
    updateUser(index:number, user: UserData) : void{}
    deleteUser(index: number): void{}
    usersCount(): number{
      return this.users.length;
    }
    loadFromLocalStorage(): void{}
    saveToLocalStorage(): void{}
  }