import { Observable } from "rxjs";
import { Country } from "src/app/interfaces/country.model";
import { State } from "src/app/interfaces/state.model";

export class CountryStatesServiceStub {

  private data = {
    "countries": [
      {
        "id": 1,
        "name": "United States"
      }],
    "states": [
      {
        "id": 1,
        "countryId": 1,
        "name": "Alabama"
      },
      {
        "id": 2,
        "countryId": 1,
        "name": "Alaska"
      }
    ]
  }

  constructor() { }

  public getCountries(): Observable<Country[]> {
    return new Observable<Country[]>((observer) => {
      setTimeout(() => {
        observer.next(this.data["countries"])
      }, 500);
    });
  }

  public getStates(countryId: number): Observable<State[]> {
    return new Observable<State[]>((observer) => {
      setTimeout(() => {
        observer.next(this.data["states"]);
      }, 500);
    });
  }
}