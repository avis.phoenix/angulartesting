import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { Router } from '@angular/router';
import { faEye, faTrash, faUserEdit } from '@fortawesome/free-solid-svg-icons';
import { UserData } from 'src/app/interfaces/user-data.model';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-single-line-user',
  templateUrl: './single-line-user.component.html',
  styleUrls: ['./single-line-user.component.scss']
})
export class SingleLineUserComponent implements OnChanges {

  @Input() myIndex: number = 0;
  @Input() canDelete: boolean = false;
  @Input() canEdit: boolean = false;

  @Output() onDelete = new EventEmitter<number>();
  
  public myUser : UserData | undefined;
  public faEye = faEye;
  public faUserEdit = faUserEdit;
  public faTrash = faTrash;

  constructor(private usersService: UsersService,
              private router: Router) {
  }

  ngOnChanges(): void {
    this.myUser = this.usersService.getUser(this.myIndex);
  }

  delete(): void{
    this.usersService.deleteUser(this.myIndex);
    this.onDelete.emit(this.myIndex);
  }

  edit(): void {
    this.router.navigate(['form', 'reactive'],{ queryParams:{ id: this.myIndex } });
  }

  view(): void{
    this.router.navigate(['view', this.myIndex]);
  }

}
