import { Component, Input, OnInit } from '@angular/core';
import { faExclamationCircle, faExclamationTriangle, faInfoCircle, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss']
})
export class ToastComponent implements OnInit {

  @Input() message:string = "";
  @Input() timeLifeSec: number = 0.5;
  public _type:string  = "normal";
  get type(): string {
    return this._type;
  }
  @Input() set type(value:string) {
    this._type = value;
    if (value == "info"){
      this.icon = faInfoCircle;
    } else if (value == "warning"){
      this.icon = faExclamationTriangle;
    } else if (value == "error"){
      this.icon = faExclamationCircle;
    } else {
      this.icon = undefined;
    }
  };

  public icon : IconDefinition | undefined;
  public see: boolean = true;

  constructor() { }

  ngOnInit(): void {
    setTimeout(()=>{
      this.see = false;
    },this.timeLifeSec*1000);
  }

}
