import { Component, Input, OnInit } from '@angular/core';
import { IconDefinition} from '@fortawesome/free-solid-svg-icons';

// Help Font Awesome: https://github.com/FortAwesome/angular-fontawesome/blob/HEAD/docs/usage.md

@Component({
  selector: 'app-big-button',
  templateUrl: './big-button.component.html',
  styleUrls: ['./big-button.component.scss']
})
export class BigButtonComponent implements OnInit {

  @Input() public myIcon : IconDefinition | undefined;
  @Input() public myTitle : string | undefined;
  @Input() public myLink : string | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
