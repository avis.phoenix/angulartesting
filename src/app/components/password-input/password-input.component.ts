import { Component, EventEmitter, Input, OnInit, Output, forwardRef } from '@angular/core';
import { IconDefinition, faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'my-password-input',
  templateUrl: './password-input.component.html',
  styleUrls: ['./password-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PasswordInputComponent),
      multi: true
    }
  ]
})
export class PasswordInputComponent implements OnInit, ControlValueAccessor {

  public visibilityIcon: IconDefinition = faEyeSlash;

  @Input() placeholder: string = "";
  @Input() set disabled(value: boolean) {
    this.setDisabledState(value);
  }
  @Output() passwordChange: EventEmitter<string> = new EventEmitter<string>();
  @Input() set password(value: string) {
    this.writeValue(value);
  }
  @Input() canShowPassword: boolean = true;

  public passwordView: string = "";
  private realPass: string = "";
  public showPass: boolean = false;
  public hasPlaceHolder: boolean = false;
  public isDisabled: boolean = false;

  public onChange: Function = (_: any) => { }
  public onTouch: Function = () => { }

  private static readonly delayCopy: number = 200;
  private static readonly delayKey: number = 100;

  constructor() { }

  ngOnInit(): void {
  }

  writeValue(value: any): void {
    if (value !== this.realPass) {
      if (value == null || value == undefined) {
        value = '';
      }
      this.realPass = value;
      this.passwordView = value;
      if (!this.showPass) {
        let len = value.length;
        setTimeout(() => {
          this.passwordView = '•'.repeat(len);
        }, PasswordInputComponent.delayCopy);
      }

      this.passwordChange.emit(this.realPass);
      this.onTouch();
      this.onChange(this.realPass);
    }
  }

  registerOnChange(fn: any): void { this.onChange = fn; }
  registerOnTouched(fn: any): void { this.onTouch = fn; }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
    if (this.showPass && isDisabled) {
      this.toggleShowPassword();
    }
  }

  onKey(event: any) {
    if (!this.showPass) {
      let posIni = Math.max(event.target.selectionStart - 1, 0);
      let posFin = event.target.value.length - (posIni + 1);
      let key = event.target.value.substr(posIni, posIni + 1);
      if (this.stringContainsOne(event.code, ['Shift'])) {
        if (key != '•') {
          this.realPass = this.realPass.substr(0, posIni) + key + this.realPass.substr(this.realPass.length - posFin);
          this.passwordChange.emit(this.realPass);
          this.onTouch();
          this.onChange(this.realPass);
          setTimeout(() => {
            this.passwordView = event.target.value.substr(0, posIni) + '•' + event.target.value.substr(posIni + 1);
          }, PasswordInputComponent.delayKey);
        }
      } else if (this.stringContainsOne(event.code, ['Key', 'Digit', 'Numpad', 'Comma', 'Period', 'Slash'])) {
        if (key === event.key || (key !== '•' && key.toUpperCase() !== event.key.toUpperCase())) {
          this.realPass = this.realPass.substr(0, posIni) + event.key + this.realPass.substr(this.realPass.length - posFin);
          this.passwordChange.emit(this.realPass);
          this.onTouch();
          this.onChange(this.realPass);
          setTimeout(() => {
            this.passwordView = '•'.repeat(posIni + 1) + event.target.value.substr(posIni + 1);
          }, PasswordInputComponent.delayKey);
        }
      } else if (event.code.search('Backspace') == 0) {
        this.realPass = this.realPass.substr(0, posIni) + this.realPass.substr(this.realPass.length - posFin);
        this.passwordChange.emit(this.realPass);
        this.onTouch();
        this.onChange(this.realPass);
      } else if (event.code.search('Delete') == 0) {
        this.realPass = this.realPass.substr(0, posIni) + this.realPass.substr(this.realPass.length - posFin);
        this.passwordChange.emit(this.realPass);
        this.onTouch();
        this.onChange(this.realPass);
      } else {
        this.passwordView = '•'.repeat(this.realPass.length);
      }
    } else {
      if (!this.stringContainsOne(event.code, ['Shift', 'Delete', 'Backspace', 'Key', 'Digit', 'Numpad', 'Comma', 'Period', 'Slash'])) {
        this.passwordView = this.realPass;
      } else {
        this.realPass = event.target.value;
        this.passwordChange.emit(this.realPass);
        this.onTouch();
        this.onChange(this.realPass);
      } 
    }


  }

  stringContainsOne(cadena: string, substrings: Array<string>): boolean {
    let i = 0;
    let salida = false;
    while (!salida && i < substrings.length) {
      salida = cadena.search(substrings[i]) == 0;
      i = i + 1;
    }
    return salida;
  }

  toggleShowPassword() {
    if (!this.disabled) {
      this.showPass = !this.showPass;
      if (this.showPass) {
        this.passwordView = this.realPass;
        this.visibilityIcon = faEye;
      } else {
        this.passwordView = '•'.repeat(this.realPass.length);
        this.visibilityIcon = faEyeSlash;
      }
    }
  }

}
