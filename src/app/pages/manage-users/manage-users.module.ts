import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageUsersRoutingModule } from './manage-users-routing.module';
import { ManageUsersComponent } from './manage-users.component';
import { SingleLineUserComponent } from 'src/app/components/single-line-user/single-line-user.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


@NgModule({
  imports: [
    CommonModule,
    ManageUsersRoutingModule,
    FontAwesomeModule
  ],
  declarations: [
    ManageUsersComponent,
    SingleLineUserComponent
  ]
})
export class ManageUsersModule { }