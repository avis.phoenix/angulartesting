import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit {

  public indexes: Array<number> | undefined;

  constructor( private usersService : UsersService) { }

  ngOnInit(): void {
    this.update();
  }

  update(){
    const n = this.usersService.usersCount();
    if (n > 0){
      this.indexes = Array.from(Array(n).keys())
    } else {
      this.indexes = [];
    }
  }

}
