import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MustMatchReactiveDirective } from 'src/app/directives/must-match.directive';
import { Country } from 'src/app/interfaces/country.model';
import { State } from 'src/app/interfaces/state.model';
import { UserData } from 'src/app/interfaces/user-data.model';
import { CountryStatesService } from 'src/app/services/country-states.service';
import { HashSha256Service } from 'src/app/services/hash-sha256.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  public userForm = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.pattern("\\b[\\w\\.-]+@[\\w\\.-]+\\.\\w{2,4}\\b")]),
    passwordGroup: new FormGroup({
      password1: new FormControl('', [Validators.required, Validators.pattern("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$")]),
      password2: new FormControl('', Validators.required)
    }, MustMatchReactiveDirective.match()),
    phone:  new FormControl('', [Validators.required, Validators.pattern("\\(\\d{3}\\)\\s\\d{3}-\\d{4}")]),
    country: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required)
  });

  public countriesData: Array<Country> | undefined;
  public statesData: Array<State> | undefined;

  public editMode = false;
  public userDoNotExist = false;
  public submitText = "Sign up";

  private realHASHPassword : string = "";
  private userID: number = -1;

  constructor(private countriesService: CountryStatesService,
              private usersService: UsersService,
              private hashSHA256: HashSha256Service,
              private router: Router,
              private activeRoute: ActivatedRoute) {
    countriesService.getCountries().subscribe((data) => {
      let userData = this.usersService.getUser(this.userID);
      this.countriesData = data;
      if (this.editMode){
        let countryID = this.countriesData?.filter(item=>item.name == userData.country)[0].id;
          if (countryID && countryID >= 0){
            this.userForm.get('country')?.setValue(countryID);

            this.countriesService.getStates(countryID).subscribe(
              (data)=>{
                this.statesData = data;
                let stateID = this.statesData?.filter(item=>item.name == userData.state)[0].id;
                if (stateID && stateID > 0){
                  this.userForm.get('state')?.setValue(stateID);
                }
              },
              (err)=>{console.error("Error to get states of country id:" + countryID + ", message: " + err )}
            );
          }
      }
    },
    (err)=>{console.error("Error to get countries: " + err)});
    
    // The url could be update 
    this.activeRoute.queryParamMap.subscribe(params =>{
      const id =params.get('id');
      if (id){
        this.userID = parseInt(id);
        this.editMode = true;
        this.submitText = "Update";

        let userData = this.usersService.getUser(this.userID);
        if (userData.password != ""){
          this.userForm.get('name')?.setValue(userData.username);
          this.userForm.get('email')?.setValue(userData.email);
          this.userForm.get('phone')?.setValue(userData.phoneNumber);
          this.userForm.get('passwordGroup')?.get('password1')?.setValue("falsePasword123");
          this.userForm.get('passwordGroup')?.get('password2')?.setValue("falsePasword123");
          this.realHASHPassword = userData.password;
          
          let countryID = this.countriesData?.filter(item=>item.name == userData.country)[0].id;
          if (countryID && countryID >= 0){
            this.userForm.get('country')?.setValue(countryID);

            this.countriesService.getStates(countryID).subscribe(
              (data)=>{
                this.statesData = data;
                let stateID = this.statesData?.filter(item=>item.name == userData.state)[0].id;
                if (stateID && stateID > 0){
                  this.userForm.get('state')?.setValue(stateID);
                }
              },
              (err)=>{console.error("Error to get states of country id:" + countryID + ", message: " + err )}
            );
          }

        } else {
          this.userForm.disable();
          this.userDoNotExist = true;
        }
      } else {
        this.editMode = false;
        this.submitText = "Sign up";
      }
    });
   }

   onSubmit() {
    if (this.countriesData && this.statesData){
      let countryID =  parseInt(this.userForm.get('country')?.value);
      let stateID =  parseInt(this.userForm.get('state')?.value);
      let user: UserData = {
        username: this.userForm.get('name')?.value,
        phoneNumber: this.userForm.get('phone')?.value,
        email:  this.userForm.get('email')?.value,
        country: this.countriesData.filter( item => item.id ==  countryID )[0].name,
        state: this.statesData.filter(item => (item.id == stateID && item.countryId == countryID ))[0].name,
        password: this.editMode ? this.realHASHPassword : this.hashSHA256.hex_sha256(this.userForm.get('passwordGroup')?.get('password1')?.value)
      }; 

      if (this.editMode){
        this.usersService.updateUser(this.userID,user);
      }else {
        this.usersService.addUser(user);
      }
      this.router.navigate(['/']);
    } else {
      throw "No country data or state data";
    }
    
  }

  ngOnInit(): void {
  }

  changeCountry(){
    if (this.userForm.get('country')?.value != ''){
      let countryID  = this.userForm.get('country')?.value;
      this.countriesService.getStates(parseInt(countryID)).subscribe((data)=>{this.statesData = data;},
      (err)=>{console.error("Error to get states of country id:" + countryID + ", message: " + err )});
    }else{
      this.statesData=undefined;
    }
  }

  prueba(val:any){
    console.log(val);
  }

}
