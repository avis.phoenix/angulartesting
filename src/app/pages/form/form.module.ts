import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormRoutingModule } from './form-routing.module';
import { FormComponent } from './form.component';
import { PasswordInputComponent } from 'src/app/components/password-input/password-input.component';
import { SharedFormModule } from 'src/app/shared-form.module';

@NgModule({
  imports: [
    CommonModule,
    FormRoutingModule,
    ReactiveFormsModule,
    SharedFormModule
  ],
  declarations: [
    FormComponent
  ]
})
export class FormModule { }