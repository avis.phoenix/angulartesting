import { HttpClientModule } from '@angular/common/http';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { PasswordInputComponent } from 'src/app/components/password-input/password-input.component';
import { CountryStatesService } from 'src/app/services/country-states.service';
import { UsersService } from 'src/app/services/users.service';
import { ActivatedRouteStub } from 'src/app/test/commons/activated-route-stub';
import { CountryStatesServiceStub } from 'src/app/test/commons/country-service-stub';
import { UsersServiceStub } from 'src/app/test/commons/users-service-stub';

import { FormComponent } from './form.component';

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;
  let debugElement: DebugElement;
  let signupService: UsersService;
  let countryService: CountryStatesService;
  let activatedRoute: ActivatedRoute;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        HttpClientModule
      ],
      providers:[
        FormComponent,
        { provide: UsersService, useClass: UsersServiceStub },
        { provide: CountryStatesService, useClass: CountryStatesServiceStub },
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ],
      declarations: [
        FormComponent,
        PasswordInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    signupService = debugElement.injector.get(UsersService);
    countryService = debugElement.injector.get(CountryStatesService);
    activatedRoute = debugElement.injector.get(ActivatedRoute);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
