import { HttpClientModule } from '@angular/common/http';
import { APP_BASE_HREF } from '@angular/common';
import { Component,DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, NgForm } from '@angular/forms';
import { Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { MustMatchDirective } from 'src/app/directives/must-match.directive';
import { CountryStatesService } from 'src/app/services/country-states.service';
import { UsersService } from 'src/app/services/users.service';
import { DisplayDataComponent } from '../display-data/display-data.component';

import { TemplateFormComponent } from './template-form.component';
import { PasswordInputComponent } from 'src/app/components/password-input/password-input.component';
import { HashSha256Service } from 'src/app/services/hash-sha256.service';
import { UsersServiceStub } from 'src/app/test/commons/users-service-stub';
import { CountryStatesServiceStub } from 'src/app/test/commons/country-service-stub';



describe('TemplateFormComponent', () => {
  let component: TemplateFormComponent;
  let fixture: ComponentFixture<TemplateFormComponent>;
  let debugElement: DebugElement;
  let formElem: DebugElement;
  let formControl: NgForm;
  let router: RouterTestingModule;
  let signupService: UsersService;
  let countriesService: CountryStatesService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        TemplateFormComponent,
        PasswordInputComponent,
        MustMatchDirective
      ],
      imports: [
        FormsModule,
        HttpClientModule,
        RouterTestingModule
      ],
      providers: [
        TemplateFormComponent,
        { provide: UsersService, useClass: UsersServiceStub },
        { provide: CountryStatesService, useClass: CountryStatesServiceStub },
        HashSha256Service
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    fixture.detectChanges();
    debugElement = fixture.debugElement;
    formElem = debugElement.query(By.directive(NgForm));
    formControl = formElem && formElem.injector.get(NgForm);
    signupService = debugElement.injector.get(UsersService);
    countriesService = debugElement.injector.get(CountryStatesService);
    router = debugElement.query(By.directive(RouterTestingModule));
  });

  beforeEach((done) => {
    fixture.whenStable().then(done);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('has a form control', () => {
    expect(formControl).toBeTruthy('form should have NgForm control');
  });

  it('should validate username is required', () => {
    const control = getFormControl('username');
    expect(control).toBeTruthy('Expected username control was not found');
    if (control) {
      control.setValue('');
      expect(control.valid).toBeFalsy('Username invalid when empty');

      control.setValue('value');
      expect(control.valid).toBeTruthy('Username valid when not empty');
    }
  });

  it('should validate email is correct', () => {
    const control = getFormControl('email');
    expect(control).toBeTruthy('Expected email control was not found');
    if (control) {
      control.setValue('test');
      expect(control.valid).toBeFalsy('Email should be invalid');

      control.setValue('test@test.com');
      expect(control && control.valid).toBeTruthy('Email should be valid');
    }
  });

  it('should validate password with requirements - at least 8 letters, numbers and uppercase', () => {
    const control = getFormControl('password');
    expect(control).toBeTruthy('Expected password control was not found');
    if (control) {
      control.setValue('abc');
      expect(control.valid).toBeFalsy('Password should be invalid');

      control.setValue('Pa55word');
      expect(control.valid).toBeTruthy('Password should be valid');
    }
  });

  it('should validate passwords match', () => {
    // get control
    const control = getFormControl('password');
    const control_match = getFormControl('password_match');

    control && control.setValue('abc1');
    fixture.detectChanges();
    control_match && control_match.setValue('abc2');
    // expect invalid
    expect(control_match && control_match.valid).toBeFalsy('Match should be invalid');
    // set value
    control && control.setValue('Pa55word');
    fixture.detectChanges();
    control_match && control_match.setValue('Pa55word');
    // expect valid
    expect(control_match && control_match.valid).toBeTruthy('Match should be valid');
  });

  it('has .form-username-error when username is invalid', () => {
    const control = getFormControl('username');

    // Make Field Valid
    control?.setErrors(null);
    control?.markAsTouched();
    fixture.detectChanges();
    expect(getFormError('username')).toBeFalsy('Error message should not be present');

    // Make field invalid
    control?.setErrors({ fake_error: true });
    control?.markAsTouched();
    fixture.detectChanges();
    expect(getFormError('username')).toBeTruthy('Error message should be present');
  });

  it('has .form-password_match-error when password_match is invalid', () => {
    const control = getFormControl('password_match');
    const control1 = getFormControl('password');

    // Make Field Valid
    control?.setErrors(null);
    control?.markAsTouched();
    control1?.markAsTouched();
    fixture.detectChanges();
    expect(getFormError('password_match')).toBeFalsy('Error message should not be present');

    // Make field invalid
    control?.setErrors({ fake_error: true });
    control?.markAsTouched();
    control1?.markAsTouched();
    fixture.detectChanges();
    expect(getFormError('password_match')).toBeTruthy('Error message should be present');
  });

  /**
   * Gets form control or undefined
   * @param name form control name
   * @returns FormControl
   */
  function getFormControl(name: string) {
    return formControl && formControl.form.get(name);
  }

  /**
   * Returns form error nativeElement for given field
   * @param fieldName
   */
  function getFormError(fieldName: string) {
    const elem = fixture.debugElement.query(
      By.css(`.form-${fieldName}-error`)
    );
    return elem && elem.nativeElement;
  }

  /**
   * @returns Array<String> array of form field names
   */
  function getFormFieldNames() {
    return formControl && Object.keys(formControl.form.controls);
  }
});