import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateRoutingModule } from './template-form-routing.module';
import { TemplateFormComponent } from './template-form.component';
import { FormsModule } from '@angular/forms';
import { MustMatchDirective } from 'src/app/directives/must-match.directive';
import { SharedFormModule } from 'src/app/shared-form.module';

@NgModule({
  imports: [
    CommonModule,
    TemplateRoutingModule,
    FormsModule,
    SharedFormModule
  ],
  declarations: [
    TemplateFormComponent,
    MustMatchDirective
  ]
})
export class TemplateFormModule { }