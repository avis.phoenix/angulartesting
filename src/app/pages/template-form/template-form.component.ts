import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Country } from 'src/app/interfaces/country.model';
import { State } from 'src/app/interfaces/state.model';
import { UserData } from 'src/app/interfaces/user-data.model';
import { CountryStatesService } from 'src/app/services/country-states.service';
import { HashSha256Service } from 'src/app/services/hash-sha256.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.scss']
})
export class TemplateFormComponent implements OnInit {

  public model = {
    username: "",
    email: "",
    password: "",
    password2: "",
    phoneNumber: "",
    countryID: "",
    stateID: "",
  }

  public countriesData: Array<Country> | undefined;
  public statesData: Array<State> | undefined;

  constructor(private countriesService: CountryStatesService,
              private usersService: UsersService,
              private hashSHA256: HashSha256Service,
              private router: Router) {
    countriesService.getCountries().subscribe((data) => {this.countriesData = data;},
    (err)=>{console.error("Error to get countries: " + err)});
   }

  onSubmit() {
    
    if (this.countriesData && this.statesData){
      let user: UserData = {
        username: this.model.username,
        phoneNumber: this.model.phoneNumber,
        email: this.model.email,
        country: this.countriesData.filter( item => item.id ==  parseInt(this.model["countryID"]) )[0].name,
        state: this.statesData.filter(item => (item.id == parseInt(this.model["stateID"]) && item.countryId == parseInt(this.model["countryID"]) ))[0].name,
        password: this.hashSHA256.hex_sha256(this.model.password)
      }; 

      this.usersService.addUser(user);
      this.router.navigate(['/']);
    } else {
      console.error("No country data or state data");
    }
    
  }

  ngOnInit(): void {

  }

  changeCountry(){
    if (this.model.countryID != ''){
      this.countriesService.getStates(parseInt(this.model.countryID)).subscribe((data)=>{this.statesData = data;},
      (err)=>{console.error("Error to get states of country id:" + this.model.countryID + ", message: " + err )});
    }else{
      this.statesData=undefined;
    }
  }

}
