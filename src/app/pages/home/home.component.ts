import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faBoxOpen, faSave, faUserPlus, faUsers } from '@fortawesome/free-solid-svg-icons';
import { NotificationItem } from 'src/app/interfaces/notification-item.model';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public options = [
    { title: "Add User - Reactive", icon: faUserPlus, link: "/form/reactive" },
    { title: "Add User - Template Build", icon: faUserPlus, link: "/form/template" },
    { title: "Manage User", icon: faUsers, link: "/manage/users"},
    { title: "Local Save", icon: faSave, link: "/service/save/browser"},
    { title: "Local Load", icon: faBoxOpen, link: "/service/load/browser"}
    /*{ title: "Remote Save", icon: faSave, link: "/manage/save/"},
     *{ title: "Remote Load", icon: faSave, link: "/manage/load/"},*/
  ];

  public notificationStack : Array<NotificationItem> = [];

  constructor( private activedRoute: ActivatedRoute,
               private usersService: UsersService) {}

  ngOnInit(): void {
    this.activedRoute.paramMap.subscribe((params)=>{
      const action = params.get('action'), type = params.get('type');

      if (action && type){
        if (action == "save" && type == "browser"){
          this.usersService.saveToLocalStorage();
          let notif: NotificationItem = {
            msg: "Sucessfully save the users data in the local storage.",
            type: "info",
            time: 3
          };
          this.notificationStack.push(notif);
        } else if (action == "load" && type=="browser"){
          try{
            this.usersService.loadFromLocalStorage();
            let notif: NotificationItem = {
              msg: "Sucessfully load the users data from the local storage.",
              type: "info",
              time: 3
            };
            this.notificationStack.push(notif);
          } catch(msg){
            let notif: NotificationItem = {
              msg: "Cannot load the users data from the local storage.",
              type: "error",
              time: 3
            };
            this.notificationStack.push(notif);
          }
          
        }
      }
    });
  }



}
