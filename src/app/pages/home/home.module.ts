import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { BigButtonComponent } from 'src/app/components/big-button/big-button.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ToastComponent } from 'src/app/components/toast/toast.component';

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    HomeRoutingModule
  ],
  declarations: [
    HomeComponent,
    BigButtonComponent,
    ToastComponent
  ]
})
export class HomeModule { }