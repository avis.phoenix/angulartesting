import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisplayDataComponent } from './display-data.component';
import { DisplayDataRoutingModule } from './display-data-routing.module';

@NgModule({
  imports: [
    CommonModule,
    DisplayDataRoutingModule
  ],
  declarations: [DisplayDataComponent]
})
export class DisplayDataModule { }