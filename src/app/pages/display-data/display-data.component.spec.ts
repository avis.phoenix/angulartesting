import { HttpClientModule } from '@angular/common/http';
import { DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { MustMatchDirective } from 'src/app/directives/must-match.directive';
import { UsersService } from 'src/app/services/users.service';

import { DisplayDataComponent } from './display-data.component';
import { ActivatedRoute, convertToParamMap } from '@angular/router';

import { UsersServiceStub } from 'src/app/test/commons/users-service-stub';


describe('DisplayDataComponent', () => {
  let component: DisplayDataComponent;
  let fixture: ComponentFixture<DisplayDataComponent>;
  
  let debugElement: DebugElement;
  let signupService: UsersService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientModule,
        RouterTestingModule,
      ],
      providers: [
        DisplayDataComponent,
        { provide: UsersService, useClass: UsersServiceStub },
        { provide: ActivatedRoute, useValue: {'snapshot': {'paramMap': convertToParamMap( {'id': '0'} )}} }
      ],
      declarations: [
        DisplayDataComponent,
        MustMatchDirective,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayDataComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    signupService = debugElement.injector.get(UsersService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  beforeEach((done) => {
    fixture.whenStable().then(done);
  });

  it('should display signup data', () => {
    let data = signupService.getUser(0);
    fixture.detectChanges();
    Object.keys(data).forEach((key) => {
      if (key != 'password'){
        const element = debugElement.query(By.css(`#${key}`)).nativeElement;
        expect(element).toBeTruthy();
        expect(element && element.textContent).toEqual(data[key as 'username' | 'phoneNumber'  | 'email' | 'country' | 'state' ]);
      }
      
    });
  });
});
