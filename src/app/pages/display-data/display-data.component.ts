import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';

import { UserData } from 'src/app/interfaces/user-data.model';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-display-data',
  templateUrl: './display-data.component.html',
  styleUrls: ['./display-data.component.scss']
})
export class DisplayDataComponent implements OnInit {

  public user: UserData | undefined;

  constructor(private usersService: UsersService,
              private activeRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    // The url do not be update 
    const id = this.activeRoute.snapshot.paramMap.get('id');
    if (id){
      this.user = this.usersService.getUser(parseInt(id));
    }
  }

}
